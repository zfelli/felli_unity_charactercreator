﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //for "restart"
using UnityEngine.UI; //for UI stuff


public class GameController : MonoBehaviour {

    //Variables to track
    public Text pageCount;
    int page = 0; //use in tandem with buttonclicked to change text, increment with new choice
    public Text gameText; //what the game "says"
    public ButtonScript[] buttons; //list of buttons, use to iterate through answers given to player
    int AI_hate = 0; //tracks how the ai feels
    public Text AIhatetext;
    bool Gender = false; //false is neutral, true is female (revealed)
    bool devtextset = false; //for dev text
    //int random; //creates random number to later be set in range 0-7
    //Text isRandom;
    //int isRandom = 0;
    //for menu system. delete if menu no longer used
    public GameObject MainScreen;
    public GameObject MenuScreen;
    public GameObject CreditScreen;
    public GameObject DevText;
    //use to track what answers players are giving
    int choice;
    public List<string> Choices;
    public Text choiceList;
    //these will basically be "switches" so each message is displayed once rather than continuously
    bool hateLVone = true;
    bool hateLVtwo = true;
    bool hateLVthree = true;
    bool hateLVfour = true;
    bool endScore = true;
    //For choices/everything
    public List<string> pg0_text;
    public List<string> pg0_choices;
    public List<string> pg1_text;
    public List<string> pg1_choices;
    public List<string> pg2_text;
    public List<string> pg2_choices;
    public List<string> pg3_text;
    public List<string> pg3_choices;
    public List<string> pg4_text;
    public List<string> pg4_choices;
    public List<string> pg5_text;
    public List<string> pg5_choices;
    public List<string> pg6_text;
    public List<string> pg6_choices;
    public List<string> pg7_text;
    public List<string> pg7_choices;
    public List<string> pg8_text;
    public List<string> pg8_choices;
    public List<string> pg9_text;
    public List<string> pg9_choices;
    public List<string> pg10_text;
    public List<string> pg10_choices;
    public List<string> end_text;
    public List<string> end_choices;
    public List<string> unstart_text;
    public List<string> unstart_choices;
    public List<string> gameover_text;
    public List<string> gameover_choices;
    public List<List<string>> Allpages_text;
    public List<List<string>> Allpages_choices;

    // Use this for initialization
    void Start() {
        //page 0
        pg0_text = new List<string>
        {"Hello. Welcome to UNTITLED; a game of fantasy, mystery, and loss. Begin?"};
        pg0_choices = new List<string>
        {"Yes","No"};
        //page 1
        pg1_text = new List<string>
        {"To begin, you must first create a character. We will start with hair. What is the color?"};
        pg1_choices = new List<string>
        {"Black", "Brown", "Blonde", "Red"};
        //page 2
        pg2_text = new List<string>
        {"What is the style?"};
        pg2_choices = new List<string>
        {"Pixie", "Buzz cut", "Short", "Medium", "Long", "Curly"};
        //page 3
        pg3_text = new List<string>
        {"How tall is your hero?" };
        pg3_choices = new List<string>
        {"Short", "Average", "Tall-ish", "Actually tall" };
        //page 4
        pg4_text = new List<string>
        {"What color are their eyes?" };
        pg4_choices = new List<string>
        {"Hazel", "Green", "Blue", "Gray", "Yellow", "Red", "Black" };
        //page 5
        pg5_text = new List<string>
        {"What is their skin color?" };
        pg5_choices = new List<string>
        {"Olive", "Black", "Mixed", "Tan", "Peach", "Pale" };
        //page 6
        pg6_text = new List<string>
        {"What is their gender?" };
        pg6_choices = new List<string>
        {"Female", "Male", "Self-Identifying" };
        //page 7
        pg7_text = new List<string>
        {"Where do they come from?" };
        pg7_choices = new List<string>
        {"Village", "Nomadic", "Small town", "Colony", "Big city" };
        //page 8
        pg8_text = new List<string>
        {"What is their personality?" };
        pg8_choices = new List<string>
        {"Confident", "Brave", "Cocky", "Nervous", "Introverted", "Sly" };
        //page 9
        pg9_text = new List<string>
        {"What do they fight for?" };
        pg9_choices = new List<string>
        {"Friends", "Family", "Themselves", "A cause" };
        //page 10
        pg10_text = new List<string>
        {"What is the strength of their conviction?" };
        pg10_choices = new List<string>
        {"Strong", "Attached", "Aware", "Weak" };
        //end
        end_text = new List<string>
        {"FIN." };
        end_choices = new List<string>
        {"PLAY AGAIN?"};
        //end page
        unstart_text = new List<string>
        {"That is unfortunate."};
        unstart_choices = new List<string>
        {"It is. May I have a second chance?", "No it isn't." };
        //game over page
        gameover_text = new List<string>
        {"GAME OVER." };
        gameover_choices = new List<string>
        {"RESTART?" };

        //All Pages
        Allpages_text = new List<List<string>> { pg0_text, pg1_text, pg2_text, pg3_text, pg4_text,
            pg5_text, pg6_text, pg7_text, pg8_text, pg9_text, pg10_text, end_text, unstart_text, gameover_text};
        Allpages_choices = new List<List<string>> {pg0_choices, pg1_choices, pg2_choices, pg3_choices, pg4_choices,
            pg5_choices, pg6_choices, pg7_choices, pg8_choices, pg9_choices, pg10_choices, end_choices, unstart_choices, gameover_choices};

        //attempt at memory?
        //PlayerPrefs.SetString("is_random", isRandom.text);
        //PlayerPrefs.SetInt("is_random", isRandom);
        //print(isRandom);

        //reset variables/counters/states
        page = 0;
        AI_hate = 0;
        gameText.text = Allpages_text[page][0]; //set starting text
        pageCount.text = "Page: " + page;
        MenuScreen.gameObject.SetActive(false);
        AIhatetext.text = "Current AI hatred level: " + AI_hate;

        //begin player choices list
        choiceList.text = "Choices: ";
        Choices = new List<string>();

        //sets random to be random
        //random = (int)(Random.Range(0f, 8f));

        //set up buttons
        for (int i = 0; i < buttons.Length; i++) //use for loop to manage 'pages' and set variables
        {
            buttons[i].gameObject.SetActive(true);
            switch (i) //switch cycles through buttons and allows to toggle on/off and change text
            {
                case 1:
                    buttons[i].setText(pg0_choices[0]);
                    break;
                case 2:
                    buttons[i].setText(pg0_choices[1]);
                    break;
                default:
                    buttons[i].gameObject.SetActive(false);
                    break;
            }
        }

	}

    public void ButtonClicked(int buttonID) //happens when a button is clicked
    {
        //add page and update pagecount
        page++;
        pageCount.text = "Page: " + page;
        //add choice to choice display
        choice = buttonID;
        if (choice <= 7) //this makes is so menu buttons (if left in game) don't affect choice list
        {
            choiceList.text += buttons[choice].buttonText.text; //adds to text display
            Choices.Add(buttons[choice].buttonText.text); //adds to list of answers
        }
        //early game end/reset loop
        if (Choices[Choices.Count - 1] == Allpages_choices[0][1]) //"No"
        {
            Submitchoice(Allpages_choices.Count - 2);
        }
        else if (Choices[Choices.Count - 1] == Allpages_choices[Allpages_choices.Count - 2][0]) //"Yes; Second chance?"
        {
            SceneManager.LoadScene("CharacterCreator");
        }
        else if (Choices[Choices.Count - 1] == Allpages_choices[Allpages_choices.Count - 2][1]) //"No it isn't."
        {
            Submitchoice(Allpages_choices.Count - 1);
        }
        else if (Choices[Choices.Count - 1] == Allpages_choices[Allpages_choices.Count - 1][0]) //"RESTART?"
        {
            SceneManager.LoadScene("CharacterCreator");
        }
        //end of game processes
        else if (Choices[Choices.Count - 1] == Allpages_choices[Allpages_choices.Count - 3][0]) //restart again
        {
            SceneManager.LoadScene("CharacterCreator");
        }
        /*else if (Choices[Choices.Count - 1] == Allpages_choices[Allpages_choices.Count - 3][1]) //continue
        {
            gameText.text += " X ";
        }*/
        else
        {
            Submitchoice(page);
        }
    }

    

    public void Submitchoice(int Test)
    {
        //changes text when player is wrong
        gameText.text = ""; //clears it so it starts blank, then adds rest of text
        if (page > 6)
        {
            Gender = true; //this basically tells the game to not use gendered phrases until after page 6 where messing up reveals the gender.
        }
        if (page > 1)
        {
            if (choice != 0)
            {
                AI_hate += 1;
                AIhatetext.text = "Current AI hatred level: " + AI_hate;
                switch (page) //adds different things to text based on what page/answer was picked
                {
                    case 2:
                        gameText.text = " Curious. Proceed. ";
                        break;
                    case 3:
                        if (Gender != true) {
                            gameText.text = " That's not their hair. ";
                        }
                        else
                        {
                            gameText.text = " That's not her hair. ";
                        }

                        break;
                    case 4:
                        if (Gender != true)
                        {
                            gameText.text = " They weren't that tall. ";
                        }
                        else
                        {
                            gameText.text = " She wasn't that tall. ";
                        }
                        break;
                    case 5:
                        gameText.text = " Not even remotely correct. ";
                        break;
                    case 6:
                        if (Gender != true)
                        {
                            gameText.text = " Have you seen them? ";
                        }
                        else
                        {
                            gameText.text = " Have you seen her? ";
                        }
                        break;
                    case 7:
                        gameText.text = " This much should be obvious by now. ";
                        break;
                    case 8:
                        gameText.text = " Wrong... ";
                        break;
                    case 9:
                        if (Gender != true)
                        {
                            gameText.text = " They were never like that. ";
                        }
                        else
                        {
                            gameText.text = " She was never like that. ";
                        }
                        break;
                    case 10:
                        gameText.text = " No. ";
                        break;
                    case 11:
                        gameText.text = " Absolutely not. ";
                        break;
                    default:
                        gameText.text = Allpages_text[Test][0];
                        break;
                }
            }
        }

        //adds additional text based on how wrong the player had been
        if (3 < AI_hate && AI_hate < 5)
        {
            if (hateLVone)
            {
                gameText.text += " You're joking, right? ";
                hateLVone = false;
            }
        }
        else if (5 <= AI_hate && AI_hate < 7)
        {
            if (hateLVtwo)
            {
                if (Gender != true)
                {
                    gameText.text += " It's like you don't even know them. ";
                }
                else
                {
                    gameText.text += " It's like you don't even know her. ";
                }
                hateLVtwo = false;
            }
        }
        else if (7 <= AI_hate && AI_hate < 9)
        {
            if (hateLVthree)
            {
                gameText.text += " That's it. There's no way you could possibly be this wrong. ";
                hateLVthree = false;
            }
        }
        else if (AI_hate >= 9)
        {
            if (hateLVfour)
            {
                if (Gender != true)
                {
                    gameText.text += " How dare you sully their name. This is nowhere even close. ";
                }
                else
                {
                    gameText.text += " How dare you sully her name. This is nowhere even close. ";
                }
                hateLVfour = false;
            }
        }

        //changes gametext based on page. previous wrong marks proceed main gametext
        gameText.text += Allpages_text[Test][0];
        if (page == 11)
        {
            if (endScore)
            {
                gameText.text += " YOUR SCORE: " + (10 - AI_hate);
                endScore = false;
            }
        }

        //random = (int)(Random.Range(0f, Allpages_choices[Test].Count)); //make random newly random
        for (int i = 0; i < Allpages_choices[Test].Count; i++) //cycles through amount of choices for given page
        {
            buttons[i].gameObject.SetActive(true);
            buttons[i].setText(Allpages_choices[Test][i]); //sets text of buttons to choicelist for that page
            //buttons[random].gameObject.SetActive(false); //randomly turns buttons off
        }
        for (int j = 7; j >= Allpages_choices[Test].Count; --j)
        {
            buttons[j].gameObject.SetActive(false); //deactivates unused buttons
        }
    }



    public void MenuButtonClicked(int buttonID) //for menu buttons; this is primarily an easy way for them to not move the page counter or further mess with the main submitchoice() function
    {
        switch (buttonID)
        {
            case 10:
                SceneManager.LoadScene("CharacterCreator"); //Reloading scene because just resetting variables and calling start() didnt actually work
                break;
            case 11: //menu (from game)
                //turn game off, menu on
                MainScreen.gameObject.SetActive(false);
                MenuScreen.gameObject.SetActive(true);
                break;
            case 12: //back to game (from menu)
                //turn menu off, game on
                MenuScreen.gameObject.SetActive(false);
                MainScreen.gameObject.SetActive(true);
                break;
            case 13: //end game
                Application.Quit();
                break;
            case 14: //credits
                MenuScreen.gameObject.SetActive(false);
                CreditScreen.gameObject.SetActive(true);
                break;
            case 15: //back to menu
                CreditScreen.gameObject.SetActive(false);
                MenuScreen.gameObject.SetActive(true);
                break;
            case 16: //toggle dev text
                //use a boolean here because dev text disappears when menus are up so I couldn't simply check if the game object was active
                if (devtextset == true)
                {
                    DevText.gameObject.SetActive(false);
                    devtextset = false;
                }
                else if (devtextset == false)
                {
                    DevText.gameObject.SetActive(true);
                    devtextset = true;
                }
                MenuScreen.gameObject.SetActive(false);
                MainScreen.gameObject.SetActive(true);
                break;
            default:
                SceneManager.LoadScene("CharacterCreator");
                break;
        }
    }
}
